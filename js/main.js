var slide_thumb = new Swiper('.slide-thumb', {
  spaceBetween: 30,
  direction: 'vertical',
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  speed: 1000,
  slidesPerView: 3,
  navigation: {
    nextEl: '.videos-recentes .area-video .btn-next',
  },
  breakpoints: {
    768: {
      direction: 'horizontal',
      slidesPerView: 1.5,
    }
  }
});

var slide_video = new Swiper('.slide-video', {
  spaceBetween: 15,
  speed: 1000,
  simulateTouch: false,
  thumbs: {
    swiper: slide_thumb
  },
  navigation: {
    nextEl: '.videos-recentes .area-video .btn-next',
  },
});

var slide_itens = new Swiper('.slide-itens', {
  spaceBetween: 15,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  speed: 500,
  slidesPerView: 5,
  navigation: {
    nextEl: '.s-conteudo .area-slide .btn-next',
  },
  breakpoints: {
    768: {
      slidesPerView: 1.5,
      spaceBetween: 5,
    }
  }
});

var slide_conteudo = new Swiper('.slide-conteudo', {
  spaceBetween: 30,
  speed: 500,
  effect: 'fade',
  simulateTouch: false,
  thumbs: {
    swiper: slide_itens
  },
  navigation: {
    nextEl: '.s-conteudo .area-slide .btn-next',
  },
  breakpoints: {
    768: {
      autoHeight: true,
    }
  }
});

var slide_artigos = new Swiper('.slide-artigos', {
  spaceBetween: 30,
  speed: 500,
  slidesPerView: 3,
  navigation: {
    prevEl: '.s-ajuda .slide-artigos .ctrl-slide .setas .btn.btn-prev',
    nextEl: '.s-ajuda .slide-artigos .ctrl-slide .setas .btn.btn-next',
  },
  pagination: {
    el: '.slide-artigos .swiper-pagination',
  },
  breakpoints: {
    768: {
      slidesPerView: 1.2,
    }
  }
});

$(".btn-ferramentas").click(function() {
  $(".dropdown-ferramentas").toggleClass("show-drop");
  $(".dropdown-artigos").removeClass("show-drop");
  $(".dropdown-aluno").removeClass("open-drop");
  return false;
})

$(".btn-artigos").click(function() {
  $(".dropdown-artigos").toggleClass("show-drop");
  $(".dropdown-ferramentas").removeClass("show-drop");
  $(".dropdown-aluno").removeClass("open-drop");
  return false;
})

$(".btn-area-aluno").click(function() {
    $(".dropdown-aluno").toggleClass("open-drop");
    $(".dropdown-artigos").removeClass("show-drop");
    $(".dropdown-ferramentas").removeClass("show-drop");
    return false;
})

$("body").click
(
  function(e)
  {
    if(e.target.className !== "dropdown-ferramentas")
    {
      $(".dropdown-artigos").removeClass("show-drop");
      $(".dropdown-ferramentas").removeClass("show-drop");
    }
  }
);

$('header nav .dropdown-aluno form .input input').focusin(function(){
  $(this).parents(".input-group").addClass("focus");
})

$('.s-sobre .texto .newsletter form input[type=text]').focusin(function(){
  $(this).parents(".form-group").addClass("focus");
})

$('header nav .dropdown-aluno form .input input').focusout(function(){
  if($(this).val() == "") {
    $(this).parents(".input-group").removeClass("focus");
  }
})

$('.s-sobre .texto .newsletter form input[type=text]').focusout(function(){
  if($(this).val() == "") {
    $(this).parents(".form-group").removeClass("focus");
  }
})

$('.box-newsletter input[type=email]').focusin(function(){
  $(this).parents(".form-group").addClass("focus");
})

$('.box-newsletter input[type=email]').focusout(function(){
  if($(this).val() == "") {
    $(this).parents(".form-group").removeClass("focus");
  }
})


$('.s-conteudo-detalhes .box-download form input[type=email]').focusin(function(){
  $(this).parents(".form-group").addClass("focus");
})

$('.s-conteudo-detalhes .box-download form input[type=email]').focusout(function(){
  if($(this).val() == "") {
    $(this).parents(".form-group").removeClass("focus");
  }
})

$(function () {
  $(window).on("scroll", function () {
    if ($(window).scrollTop() > 100) {
      $("header").addClass("fixed-header");
    } else {
      $("header").removeClass("fixed-header");
    }
  });
});

$(".btn-mostrar-dados-tabela").click(function() {
  $("#printfinal").addClass("show-all");
  $(this).hide();
})

$('.s-conteudo-taxa .topo-indice .orientacao .guia ul li a')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 100
                }, 1000);
            }
        }
    });

    $('.btn-grafico-completo')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 100
                }, 1000);
            }
        }
    });

  $(".response-success .btn-back").on("click", function() {
    $(this).parents(".response-success").hide();
    $(".form-default").fadeIn();
    $("#inputNome").val("");
    $("#inputEmail").val("");
    $("#inputMensagem").val("");
    return false;
  })

  $('#formContato').validate({
    rules: {
        nome: 'required',
        email: {
            required: true,
            email: true
        },
        mensagem: 'required'
    },
    messages: {
        nome: 'Nome é obrigatório',
        email: {
            required: 'Email é obrigatório'
        },
        mensagem: {
            required: 'Mensagem é obrigatório'
        }
    }
});
var formContato = $('#formContato');

formContato.on('submit', function (e) {
    e.preventDefault();
    if (formContato.valid()) {
        $(".s-contato .box-white form .form-group input[type=submit]").addClass('btn-disabled');
        $(".s-contato .box-white form .form-group input[type=submit]").val('AGUARDE...');
        var url = "./contato.php";
        var inputNome = $("#inputNome").val();
        var inputEmail = $("#inputEmail").val();
        var inputMensagem = $("#inputMensagem").val();
        var dados = {
            nome: inputNome,
            email: inputEmail,
            mensagem: inputMensagem
        };

        

        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: dados,
            async: false,
            timeout: 10000,
            success: function (data) {
                if (data == "true") {
                    $(".s-contato .box-white form .form-group input[type=submit]").removeClass('btn-disabled');
                    $(".s-contato .box-white form .form-group input[type=submit]").val('ENVIAR');
                    $(".form-default").hide();
                    $(".response-success").show();
                }
            },
            error: function (x, t, m) {
                alert("Erro ao enviar! Entre em contato com o desenvolvedor do site.");
                $(".s-contato .box-white form .form-group input[type=submit]").removeClass('btn-disabled');
                $(".s-contato .box-white form .form-group input[type=submit]").val('ENVIAR');
            }
        });
    }
});

$(".menu-button").click(function(){
  $(".menu-mobile").fadeToggle();

  return false;
})

const menu = () => {
  const menuButton = document.getElementById('js-open-menu');
  menuButton.addEventListener('click', (e) => {
    e.preventDefault();
    document.documentElement.classList.toggle('menu-opened');
  });
};
menu();

AOS.init();