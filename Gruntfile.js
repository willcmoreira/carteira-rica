module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css/',
                    ext: '.min.css'
                }]
            }
        },
        connect: {
            sever: {
                options: {
                    hostname: 'localhost',
                    port: 3000,
                    base: './',
                    livereload: true
                }
            }
        },
        watch: {
            options: {
                livereload: true,
                dateFormat: function(time) {
                    grunt.log.writeln('O relógio terminou em ' + time + 'ms, no dia ' + (new Date()).toString());
                    grunt.log.writeln('Aguardando alteraçoes...');
                }
            },
            less: {
                files: ['less/*.less'],
                tasks: ['less', 'cssmin']
            }
        },
        less: {
            development: {
                options: {
                    paths: ['less/']
                },
                files: {
                    'css/main.css': 'less/main.less'
                }
            }
        },
        uglify: {
            build: {
                files: [{
                    src: 'js/main.js',
                    dest: 'js/main.min.js'
                }]
            }
        },
    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');


    grunt.registerTask('default', ['cssmin','connect','watch','uglify']);
};
